<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <title>SIMPLE LARAVEL 9 CRUD IN HINDI</title>
</head>
<body>
    <div class="bg-dark py-3">
        <div class="container">
            <div class="h4 text-white">SIMPLE LARAVEL 9 CRUD IN HINDI</div>
        </div>
    </div>

    <div class="container">
        <div class="d-flex justify-content-between py-3">
            <div class="h5">Employees</div>
            <div>
                <a href="{{route('employees.create')}}" class="btn btn-primary">Create</a>
            </div> 
        </div>
        <div>
        @if(Session::has('success'))
                <div class="alert alert-success">
                    {{Session::get('success')}}
                </div>
            @endif
        </div>
        <div class="card border-0 shadow-lg">
            <div class="card-body">
                <table class="table table-striped">
                    <tr>
                        <th>ID</th>
                        <th>Image</th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Address</th>
                        <th>Action</th>
                    </tr>
                    @if($employees->isNotEmpty())
                    @foreach ($employees as $employee)
                    <tr valign="middle">
                        <td>{{$employee->id}} </td>
                        <td>
                            @if($employee->image != '' && file_exists(public_path().'/uploads/employees/'.$employee->image))
                            <img src="{{url('uploads/employees/'.$employee->image)}}" alt="" width="40" height="40" class="rounded-circle">
                            @else
                            @endif
                        </td>
                        <td>{{$employee->name}}</td>
                        <td>{{$employee->email}}</td>
                        <td>{{$employee->address}}</td>
                        <td>
                            <a href="{{route('employees.edit',$employee->id)}}" class="btn btn-primary btn-sm">Edit</a>
                            <a href="#" onclick="deleteEmployee({{$employee->id}})" class="btn btn-danger btn-sm">Delete</a>
                            <form  id="employee-edit-action-{{$employee->id}}" action="{{route('employees.destroy', $employee->id)}}" method="post">
                                @csrf
                                @method('delete')
                            </form>
                        </td>
                    </tr>
                    @endforeach
                   
                    @else
                    <tr>
                        <td colspan="6">Racord Not Found</td>
                    </tr>
                    @endif
                </table>
            </div>
        </div>
        <div class="mt-3">
            {{$employees->links()}}
        </div>

    </div>
</body>
</html>
<script>
    function deleteEmployee(id){
        if(confirm("Are you sure you want to delete ?")){
            document.getElementById('employee-edit-action-'+id).submit();
        };
    }
</script>